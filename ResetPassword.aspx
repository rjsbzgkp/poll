﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container" id="divMainRP" style="display:none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4" style="background-color: white;padding: 20px;">
                    <div class="row mb-3">
                       <h2>Reset Password</h2>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-4 col-form-label labal">New Password <span style="color: red;">*</span></label>
                        <div class="col-sm-8 pr-49">
                            <input class="form-control req" type="password" placeholder="Enter Password " id="txtPassword">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-4 col-form-label labal">Confirm Password <span style="color: red;">*</span></label>
                        <div class="col-sm-8 pr-49">
                            <input class="form-control req" type="password" placeholder="Enter Confirm Password " id="txtConfirmPassowrd">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-12 pr-49" style="text-align: right;">
                            <button type="button" id="btnChangePsw" onclick="ChangePassword()" class="btn btn-success btn-sm">Change</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
</asp:Content>

