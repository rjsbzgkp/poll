﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ResetPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string script = File.ReadAllText(Server.MapPath("/CustomJS/ResetPassword.js"));
        Page.Header.Controls.Add(
             new System.Web.UI.LiteralControl(
                "<script>" + script + "</script>"
            ));
    }
}