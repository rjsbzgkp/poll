create table AnswerType(
AutoId int identity(1, 1) primary key not null,
QuestionAutoId int not null,
Name varchar(500),
CreatedDate datetime,
UpdatedDate datetime,
Status bit
)