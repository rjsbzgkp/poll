create table QuestionMaster(
AutoId int identity(1, 1) primary key not null,
UserAutoId int not null,
Question varchar(1000),
DisplayResults bit,
AllowComments bit,
allowGuests bit,
allowMultiSelection bit,
CreatedDate datetime,
UpdatedDate datetime,
Status bit
)