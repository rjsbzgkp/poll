create table UserMaster(
AutoId int identity(1, 1) primary key not null,
Name varchar(70),
Email varchar(max),
Password varbinary(max),
CreatedDate datetime,
Status bit
)