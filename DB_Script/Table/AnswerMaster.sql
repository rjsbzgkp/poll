select um.Name,qm.AutoId,qm.UserAutoId,qm.Question, 
				case when qm.DisplayResults = 1 then 'YES' else 'NO' end as DisplayResults,
				case when qm.AllowComments = 1 then 'YES' else 'NO' end as AllowComments,
				case when qm.allowGuests = 1 then 'YES' else 'NO' end as allowGuests,
				case when qm.allowMultiSelection =1 then 'YES' else 'NO' end as allowMultiSelection,
				FORMAT (qm.CreatedDate, 'MMM dd yyyy') as CreatedDate, FORMAT (qm.UpdatedDate, 'MMM dd yyyy') as UpdatedDate,case when qm.Status =1 then '1' else '0' end as Status from QuestionMaster qm
				inner join UserMaster um on um.AutoId =qm.UserAutoId
				where qm.AutoId = 5