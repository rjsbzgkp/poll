  
CREATE OR ALTER  PROCEDURE [dbo].[ProcCreatePoll]    
@Opcode int=NULL,  
@AutoId int=null,  
@Pollid varchar(100)=null,  
@Name varchar(100)=null,  
@Email varchar(100)=null,  
@Password varchar(max)=null,  
@Question varchar(max)=null,  
@pollstatus varchar(max)=null,  
@todate varchar(max)=null,  
@fromdate varchar(max)=null,  
@AnsTypeName varchar(max)=null,  
@DisplayResults bit=null,  
@AllowComments bit=null,  
@allowGuests bit=null,  
@QuestionAutoId int = null,  
@votingdetails xml= null,  
@Poll bit=null,  
@allowMultiSelection bit=null,  
@Status bit=null,  
@isException bit out,    
@exceptionMessage varchar(max) out    
AS    
BEGIN    
 BEGIN TRY    
  SET @exceptionMessage= 'Success'    
  SET @isException=0    
  declare @UserId int  
  IF @Opcode=11    
  BEGIN   
     IF EXISTS(SELECT top 1 AutoId from UserMaster where Email=@Email AND ISNULL(@AutoId,0)=0)  
     BEGIN  
         SET @isException=1  
      SET @exceptionMessage='Exists'  
     END  
     ELSE  
     BEGIN   
     BEGIN TRAN  
  
        IF ISNULL(@Email,'')!=''  
        BEGIN  
       set @AutoId =(SELECT top 1 AutoId from UserMaster where Email=@Email)  
           END  
  
      IF isnull(@AutoId,0) = 0  
      BEGIN  
       insert into UserMaster(Name,Email,Password,CreatedDate,Status)  
       values (@Name,@Email,CONVERT(varbinary(max),ENCRYPTBYPASSPHRASE('POLL', @Password)),GETDATE(),1)  
       SET @UserId=(SELECT SCOPE_IDENTITY())  
      END  
      ELSE  
      BEGIN  
       SET @UserId = @AutoId  
      END  
  
      insert into QuestionMaster(Pollid,UserAutoId,Question,DisplayResults,AllowComments,allowGuests,allowMultiSelection,CreatedDate,Status)  
      values (@Pollid,@UserId,@Question,@DisplayResults,@AllowComments,@allowGuests,@allowMultiSelection,GETDATE(),1)  
  
      declare @QuestionId int = (SELECT SCOPE_IDENTITY())  
      insert into AnswerType(QuestionAutoId,Name,CreatedDate,Status)  
      SELECT @QuestionId,tbl1.splitdata,GETDATE(),1 FROM (SELECT * FROM [dbo].[fnSplitString](@AnsTypeName,','))  AS tbl1  
        
      IF isnull(@AutoId,0) = 0  
      BEGIN  
       SELECT CONVERT(varchar(max),decryptbypassphrase ('POLL',Password)) as Pass,   AutoId,Name,Email from UserMaster where AutoId = @UserId  
      END  
      ELSE  
      BEGIN  
       SELECT AutoId,Name,Email from UserMaster where AutoId = @UserId  
      END  
     COMMIT TRAN  
     END  
  END  
  IF @Opcode=41  
  BEGIN  
  
   select QM.Pollid,QM.AutoId, UM.Name,QM.Question,case when QM.Status=1 then '1' else '0' end as sts,CONVERT(varchar,QM.CreatedDate, 0) as CreatedDate from QuestionMaster as QM   
   inner join UserMaster as UM on UM.AutoId = QM.UserAutoId  
   where (QM.UserAutoId = @AutoId /*or qm.AutoId in (select am.QuestionAutoId from AnswerMaster as am where am.UserAutoId =@AutoId)*/)  
   and (@fromdate='' or @fromdate is null or @todate='' or @todate is null or  (cast (QM.CreatedDate as date)  between @fromdate and @todate))   
   and (@pollstatus='' or @pollstatus is null or @pollstatus='3' or Qm.Status = @pollstatus) order by QM.CreatedDate desc  
  END  
  IF @Opcode=42  
  BEGIN  
   IF EXISTS(select AutoId from UserMaster where Email = @Email and DecryptByPassphrase('POLL',Password) = @Password)  
   BEGIN  
    select AutoId,Name,Email from UserMaster where Email = @Email and DecryptByPassphrase('POLL',Password) = @Password  
   END  
   ELSE  
   BEGIN  
    SET @isException=1    
    SET @exceptionMessage= 'no record'   
   END  
  END  
  --Showpoll  
  if @Opcode=43  
  BEGIN   
   BEGIN TRY  
    select qm.Pollid,case when @AutoId=um.AutoId then um.Name+' (Your)' else um.Name end as Name,qm.AutoId,qm.UserAutoId,qm.Question,   
    case when qm.DisplayResults = 1 then 'YES' else 'NO' end as DisplayResults,  
    case when qm.AllowComments = 1 then 'YES' else 'NO' end as AllowComments,  
    case when qm.allowGuests = 1 then 'YES' else 'NO' end as allowGuests,  
    case when qm.allowMultiSelection =1 then 'YES' else 'NO' end as allowMultiSelection,  
    FORMAT (qm.CreatedDate, 'MMM dd yyyy') as CreatedDate, FORMAT (qm.UpdatedDate, 'MMM dd yyyy') as UpdatedDate,case when  
    qm.Status =1 then '1' else '0' end as Status,  
    case when ISNULL(@AutoId,0)=qm.UserAutoId then 1 else 0 end as ligaluser  from QuestionMaster qm  
    inner join UserMaster um on um.AutoId =qm.UserAutoId  
    where qm.Pollid = @Pollid order by um.Name asc  
  
    select at.name,at.AutoId from QuestionMaster qm  
    inner join AnswerType at on at.QuestionAutoId = qm.AutoId  
    where qm.Pollid = @Pollid  
    SELECT [id],[URL]FROM [dbo].[PollUrl]  
    select QM.Pollid,AM.AutoId,AM.Comments,AM.Kids,AM.Adults,AM.Name as chechbox,AM.QuestionAutoId,AM.UserAutoId,UM.Name from [dbo].[AnswerMaster] AM  
    inner join [dbo].[QuestionMaster] QM on QM.AutoId = AM.QuestionAutoId  
   -- inner join [dbo].[AnswerType] AT on AT.QuestionAutoId = QM.AutoId  
    inner join [dbo].[UserMaster] UM on UM.AutoId = AM.UserAutoId  
    where QM.Pollid = @Pollid and UM.AutoId = @AutoId  
   END TRY   
   BEGIN CATCH  
    SET @isException=1    
    SET @exceptionMessage= 'no record'   
   END CATCH  
  END  
	--createvote  
	if @Opcode=44  
	BEGIN
		BEGIN TRY
		IF EXISTS(SELECT AutoId from AnswerMaster where Name=trim(@Name) AND QuestionAutoId=@QuestionAutoId) 
		BEGIN 
		     SET @isException=1
			 SET @exceptionMessage='DuplicatePoll'
        END
		ELSE
		BEGIN
			IF @AutoId = 0  
			BEGIN  
				IF NOT EXISTS(SELECT AutoId from UserMaster where Email=@Email) OR trim(@Email)=''  
				BEGIN  
					insert into UserMaster(Name,Email,Password,CreatedDate,Status)  
					values (@Name,@Email,CONVERT(varbinary(max),ENCRYPTBYPASSPHRASE('POLL', @Password)),GETDATE(),1)  
					SET @AutoId=(SELECT SCOPE_IDENTITY())  
				END  
				ELSE IF EXISTS(SELECT AutoId from UserMaster where Email=@Email) OR trim(@Email)=''
				BEGIN
				    
				END
				BEGIN  
					SELECT @AutoId=AutoId,@Password=convert(varchar(100),DecryptByPassphrase('POLL',Password)) from UserMaster where Email=@Email   
					DELETE FROM [dbo].[AnswerMaster] WHERE QuestionAutoId = @QuestionAutoId and UserAutoId = @AutoId  
				END  
			END  

			INSERT INTO AnswerMaster(QuestionAutoId,UserAutoId,Name,Comments,Adults,Kids,AtAutoId,CreatedDate,UpdatedDate,Status)  
			SELECT @QuestionAutoId,@AutoId,  
			x.value('name[1]', 'varchar(max)') AS name,  
			x.value('coment[1]', 'varchar(max)') AS coment,  
			x.value('adult[1]', 'int') AS adult,  
			x.value('kids[1]', 'int') AS kids,   
			x.value('atid[1]', 'int') As atid,  
			GETDATE(),GETDATE(),1  
			FROM @votingdetails.nodes('//vote') XmlData(x)  
			select @AutoId as AutoId,@Password as Password
		END	
		END TRY  
		BEGIN CATCH  
			SET @isException=1    
			SET @exceptionMessage= 'vote failed'   
		END CATCH  
	END  
--Displayresult  
  if @Opcode=45  
  BEGIN    
   BEGIN TRY  
    select QM.Pollid,AM.AutoId,AM.Comments,AM.Kids,AM.Adults,AM.Name as chechbox,AM.QuestionAutoId,AM.UserAutoId,QM.Question,UM.Name,  
    case when ISNULL(@AutoId,0)=QM.UserAutoId then 1 else 0 end as legaluser from [dbo].[AnswerMaster] AM  
    inner join [dbo].[QuestionMaster] QM on QM.AutoId = AM.QuestionAutoId  
   -- inner join [dbo].[AnswerType] AT on AT.QuestionAutoId = QM.AutoId  
    inner join [dbo].[UserMaster] UM on UM.AutoId = AM.UserAutoId  
    where QM.Pollid = @Pollid   
  
    select qm.DisplayResults,at.name,at.AutoId, case when ISNULL(@AutoId,0)=qm.UserAutoId then 1 else 0 end as legaluser from QuestionMaster qm  
    inner join AnswerType at on at.QuestionAutoId = qm.AutoId  
    where qm.Pollid =  @Pollid  
   END TRY  
    BEGIN CATCH  
     SET @isException=1    
     SET @exceptionMessage= 'no record'   
    END CATCH  
  END  
  --editquestion  
  if @Opcode=46  
  BEGIN   
   BEGIN TRY  
    select um.Name,qm.AutoId,qm.UserAutoId,qm.Question,   
    case when qm.DisplayResults = 1 then 'YES' else 'NO' end as DisplayResults,  
    case when qm.AllowComments = 1 then 'YES' else 'NO' end as AllowComments,  
    case when qm.allowGuests = 1 then 'YES' else 'NO' end as allowGuests,  
    case when qm.allowMultiSelection =1 then 'YES' else 'NO' end as allowMultiSelection,  
    FORMAT (qm.CreatedDate, 'MMM dd yyyy') as CreatedDate, FORMAT (qm.UpdatedDate, 'MMM dd yyyy') as UpdatedDate,case when qm.Status =1 then '1' else '0' end as Status from QuestionMaster qm  
    inner join UserMaster um on um.AutoId =qm.UserAutoId  
    where qm.AutoId = @QuestionAutoId  
  
    select at.name,at.AutoId from QuestionMaster qm  
    inner join AnswerType at on at.QuestionAutoId = qm.AutoId  
    where qm.AutoId =  @QuestionAutoId  
   END TRY  
   BEGIN CATCH  
    SET @isException=1    
    SET @exceptionMessage= 'no record'   
   END CATCH  
  END  
  --updatePoll  
  if @Opcode=47  
  BEGIN   
   BEGIN TRY  
   UPDATE [dbo].[QuestionMaster] SET [Question] = @Question,[DisplayResults] = @DisplayResults,[AllowComments] = @AllowComments,[allowGuests] = @allowGuests,[allowMultiSelection] = @allowMultiSelection,[UpdatedDate] = GETDATE() WHERE AutoId = @QuestionAutoId  
  
    INSERT INTO AnswerType(QuestionAutoId,Name,CreatedDate,UpdatedDate,Status)  
    select @QuestionAutoId,t.name,GETDATE(),GETDATE(),1 from (  
    select   
    x.value('name[1]', 'varchar(max)') AS name  
    FROM @votingdetails.nodes('//vote') XmlData(x)  
    ) as t where not exists (select 1 from AnswerType as aty where QuestionAutoId=@QuestionAutoId and aty.Name=t.name)  
           UPDATE aty set aty.Name=t.name,UpdatedDate=GETDATE() from (  
    select   
    x.value('atid[1]', 'varchar(max)') AS atAutoid,  
    x.value('name[1]', 'varchar(max)') AS name  
    FROM @votingdetails.nodes('//vote') XmlData(x)  
    ) as t   
    inner join AnswerType as aty on aty.QuestionAutoId=@QuestionAutoId and t.atAutoid=aty.AutoId  
  
    DELETE FROM AnswerType  where QuestionAutoId=@QuestionAutoId and name not in (  
    select   
    x.value('name[1]', 'varchar(max)') AS name  
    FROM @votingdetails.nodes('//vote') XmlData(x)  
    )    
    END TRY  
    BEGIN CATCH  
     SET @isException=1    
     SET @exceptionMessage= 'Poll update failed'   
    END CATCH  
  END  
  --getpolldet  
  if @Opcode=48  
  BEGIN  
   BEGIN TRY  
    SELECT [AutoId],[ATAutoId],[QuestionAutoId],[UserAutoId],[Name],[Comments],[Adults],[Kids],[CreatedDate],[UpdatedDate],[Status]  
      FROM [dbo].[AnswerMaster] where QuestionAutoId = @QuestionAutoId and UserAutoId = @AutoId  
   END TRY  
   BEGIN CATCH  
    SET @isException=1    
    SET @exceptionMessage= 'No Data'   
   END CATCH  
  END  
  --updateVote  
  if @Opcode=49  
  BEGIN  
   BEGIN TRY  
    DELETE FROM [dbo].[AnswerMaster] WHERE QuestionAutoId = @QuestionAutoId and UserAutoId = @AutoId  
    INSERT INTO AnswerMaster(QuestionAutoId,UserAutoId,Name,Comments,Adults,Kids,AtAutoId,CreatedDate,UpdatedDate,Status)  
   SELECT @QuestionAutoId,@AutoId,  
   x.value('name[1]', 'varchar(max)') AS name,  
            x.value('coment[1]', 'varchar(max)') AS coment,  
   x.value('adult[1]', 'int') AS adult,  
   x.value('kids[1]', 'int') AS kids,  
   x.value('atid[1]', 'int') As atid,  
   GETDATE(),GETDATE(),1  
   FROM @votingdetails.nodes('//vote') XmlData(x)  
   END TRY  
   BEGIN CATCH  
    SET @isException=1    
    SET @exceptionMessage= 'vote failed'   
   END CATCH  
  END  
  -- change status  
  if @Opcode=50  
  BEGIN  
   BEGIN TRY  
    UPDATE [dbo].[QuestionMaster]  
    SET Status =  
       case   
       when Status=1 then 0    
       else 1  end  
    where Pollid = @Pollid  
   END TRY  
   BEGIN CATCH  
    SET @isException=1    
    SET @exceptionMessage= 'status update failed'   
   END CATCH  
  END  
  IF @Opcode=51  
  BEGIN  
        IF EXISTS(Select autoId from UserMaster where Email=@Email)  
     BEGIN  
      Insert into ForgotPasswordVerification (EmailId,Status,CreateDate)   
      Select Email,0,getdate() from UserMaster where Email=@Email  
  
      SET @AutoId=SCOPE_IDENTITY()  
        
       Select @AutoId as AutoId,Name,Email from UserMaster where Email=@Email  
     END  
     ELSE  
     BEGIN  
       SET @isException=1  
       SET @exceptionMessage='User does not exists.'  
     END  
  END  
  IF @Opcode=52  
  BEGIN  
        IF EXISTS(Select autoId from ForgotPasswordVerification where AutoId=@AutoId)  
     BEGIN  
         SET @Email=(Select top 1 EmailId From ForgotPasswordVerification  where AutoId=@AutoId)  
       Update UserMaster SET Password=CONVERT(varbinary(max),ENCRYPTBYPASSPHRASE('POLL', @Password)) Where Email=@Email  
       Update ForgotPasswordVerification SET Status=1 where EmailId=@Email AND AutoId=@AutoId  
     END  
     ELSE  
     BEGIN  
       SET @isException=1  
       SET @exceptionMessage='Unauthorizes Access'  
     END  
  END  
  IF @Opcode=53  
  BEGIN    
     IF EXISTS(Select EmailId from ForgotPasswordVerification where AutoId=@AutoId AND Status=1)  
     BEGIN  
       SET @isException=1  
       SET @exceptionMessage='PasswordChanged'  
     END  
        ELSE IF NOT EXISTS(Select EmailId from ForgotPasswordVerification where AutoId=@AutoId AND Status=0)  
     BEGIN  
       SET @isException=1  
       SET @exceptionMessage='UnauthorizesAccess'  
     END  
      
  END  
  END TRY  
  BEGIN CATCH    
   ROLLBACK TRAN  
   SET @isException=1    
   SET @exceptionMessage= ERROR_MESSAGE()    
 END CATCH   
END 