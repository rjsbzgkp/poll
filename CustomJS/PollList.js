﻿$(document).ready(function () {
    GetPollQue();
})


function GetPollQue() {
    debugger;
    var data = {
        pollstatus: $("#status").val().trim(),
        fromdate: $("#fromDate").val().trim(),
        todate: $("#toDate").val().trim()
    };
    $.ajax({
        type: "POST",
        url: "/PollList.aspx/Getalldata",
        data: JSON.stringify({ pobj: data }),
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (response) {
            if (response.d != "Session Expired") {
                if (response.d == "false") {
                    location.href = '/Default.aspx';
                }
                else {
                    debugger;
                    var XML = $.parseXML(response.d);
                    var datas = $(XML).find("Table");
                    var html = '';
                    var k = 0;
                    if (datas.length > 0) {
                        $.each(datas, function (i) {
                            $('#countQue').html('Total Poll - '+(i + 1));
                            //i++;
                            var sts = '';
                            if ($(this).find("sts").text() == 1) {
                                sts = '<span class="badge badge-secondary">Open</span>';
                            }
                            else {
                                sts = '<span class="badge badge-danger">Close</span>';
                            }
                            html += '<div class="card p-3" onclick="showpoll(\'' + $(this).find("Pollid").text() + '\')"><h3 style="text-align: left"><pre style="text-align:left;line-height:1;overflow: unset;color:#303e67;font-weight:800;font-size:1rem" class="text-primary">' + $(this).find("Question").text() + '</pre></h3><p><span><b>   Created By: </b>' + $(this).find("Name").text() + '</span><span><b>   Created On: </b>' + $(this).find("CreatedDate").text() + '</span><b>    Status: </b>' + sts + '</p></div>';
                        });
                        $('#Polllist').html(html);
                    }
                }
            }
            else {
                swal({
                    closeOnClickOutside: false,
                    text: "Session expired",
                }).then(function (isConfirm) {
                    location.href = '/Default.aspx';
                })
            }
        },
        error: function (result) {
            swal("", "Error in syntax", "error");
        },
        failure: function (result) {
            swal("", 'Oops,some thing  wrong.Please try again.', "error");
        }
    });
}
function showpoll(pollid) {

    localStorage.setItem('htmldata', pollid);

    location.href = 'showPoll.aspx?id=' + pollid;

}

function getfilterdata() {
    debugger;
    GetPollQue()
}
