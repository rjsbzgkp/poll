﻿$(document).ready(function () {
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    showpoll(getQueryString('id'));
    Displayresult(getQueryString('id'));
});

function validateRe() {
    var Questions = $('#UserQust').val().trim();
    if (session != 'yes') {
        var name = $('#UserName1').val().trim();
        var email = $('#UserEmail1').val().trim().toLowerCase();
        if (name == null || name == "") {
            $('#UserName').addClass('border-warning');
            $('#UserName').focus();
            fire_toast("All * fields are mandatory.", "error");
            return false;
        }
        else {
            $('#UserName').removeClass('border-warning');
        }
        //if (email == null || email == "") {
        //    $('#UserEmail').addClass('border-warning');
        //    $('#UserEmail').focus();
        //    fire_toast("All * fields are mandatory.", "error");
        //    return false;
        //}
        //else {
        //if (email != null || email != "") {
        //    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        //    if (email.match(mailformat)) {
        //        $('#UserEmail').removeClass('border-warning');
        //    }
        //    else {
        //        $('#UserEmail').addClass('border-warning');
        //        $('#UserEmail').focus();
        //        fire_toast("Please enter valid Email ID.", "error");
        //        return false;
        //    }
        //}
        //}
    }

    return true;
}
function showpoll(pid) {

    $.ajax({
        url: 'showPoll.aspx/showpoll',
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        data: "{pollid : '" + pid + "'}",
        success: function (ds) {
            if (ds != "") {
                var xmldoc = $.parseXML(ds.d)
                var table1 = $(xmldoc).find('Table');
                var table2 = $(xmldoc).find('Table1');
                var url = $(xmldoc).find('Table2');
                var userresult = $(xmldoc).find('Table3');
                var html = '';
                var data = '';
                var sts = '';

                if ($(table1).find("Status").text() == 1) {
                    sts = '<span id="status" class="badge badge-secondary">Open</span>';
                }
                else {
                    sts = '<span class="badge badge-danger">Close</span>';
                }
                var uservote = '';
                $.each(userresult, function (key, value) {
                    uservote += '<tr><td style="color:black;text-align:center;">' + $(value).find("chechbox").text() + '</td></tr>';
                })
                var pollid = $(table1).find("Pollid").text();
                var pollurl = $(url).find("URL").text();
                var votebtn = '';
                if ($(table1).find("ligaluser").text() == 1) {
                    $("#chngsts").show();
                    $("#modaledit").show();
                }
                if ($(table1).find("Status").text() == 1) {
                    var votebtn = '<div class="btnM"><button class="btn btn-primary" type="button" data-bs-toggle="modal" onclick="getpolldetails()" data-bs-target="#Vote" href="javascript:void(0)"><span><i class="fa fa-edit"></i> Vote</span></button></div>';
                }
                debugger

                html += '<div class="card p-3"><p><b>Link: </b>' + '' + pollurl + '' + pollid +
                    '&nbsp&nbsp<button class="btn btn-primary btn-sm copy-text" type="button" alt="copy link to this poll" title="copy this poll" onclick="copyToClipboard()"><i class="fa fa-copy"></i></button></p>'
                    + '<input type="text" style="display:none" id="pollid" value="' + pollurl + '' + pollid + '" readonly="">'
                    + '<input type="text" style="display:none" id="polid" value="' + pollid + '" readonly="">'

                    + '<div class="row">'
                    + '<div class="col-md-3"><p> <b>Status: </b>' + sts + '</p ></div>'
                    + '<div class="col-md-3"><p><b>Created By : </b>' + $(table1).find("Name").text() + '</p></div>'
                    + '<div class="col-md-3"><p><b>Created On : </b>' + $(table1).find("CreatedDate").text() + '</p></div>'
                    + '<div class="col-md-3"></div>'
                    + '</div>';

                    //+ '<p> <b>Status: </b>' + sts + '</p >' +
                    //'<p><b>   Created By : </b>' + $(table1).find("Name").text() + '</p>' +
                    //'<p><b>   Created On : </b>' + $(table1).find("CreatedDate").text() + '</p>';
                if ($(table1).find("ligaluser").text() == 1) {

                    //html += '<p><b>Display Results To Everyone? : </b>' + $(table1).find("DisplayResults").text() + '</p>' +
                    //    '<p><b>Allow Comments? : </b>' + $(table1).find("AllowComments").text() + '</p>' +
                    //    '<p><b>Allow Adults + Kids selection? : </b>' + $(table1).find("allowGuests").text() + '</p>' +
                    //    '<p><b>Allow MultiSelection? : </b>' + $(table1).find("allowMultiSelection").text() + '</p><hr style="border-top:1px solid black">';

                    html += '<div class="row">'
                        + '<div class="col-md-3"><p><b>Display Results To Everyone? : </b>' + $(table1).find("DisplayResults").text() + '</p></div>'
                        + '<div class="col-md-3"><p><b>Allow Comments? : </b>' + $(table1).find("AllowComments").text() + '</p></div>'
                        + '<div class="col-md-3"><p><b>Allow Adults + Kids selection? : </b>' + $(table1).find("allowGuests").text() + '</p></div>'
                       
                        + '</div>'
                        + '<p><b>Allow MultiSelection? : </b>' + $(table1).find("allowMultiSelection").text() + '</p>';
                }
                html += '<pre style="text-align: left;line-height:1.5;color: #303e67;margin-top:15px;font-size:1rem;font-family: Roboto,sans-serif;background-color: #e1e1e1;padding:5px;overflow: hidden;white-space:break-spaces;">' + $(table1).find("Question").text() + '</pre>' +
                    '<table class="table table-bordered mb-1 "><tbody>' + uservote + '</tbody></table>' + votebtn + ''
                '</div>';
                $('#Userid').val($(table1).find("UserAutoId").text());
                $('#QueID').val($(table1).find("AutoId").text());
                $('#Uname').val($(table1).find("Name").text());
                var vote = '';
                var comment = '';
                var Button = "";
                var MultiSelection = "";
                if ($(table1).find("AllowComments").text() == "YES") {
                    comment = '<textarea class="coment" name="txtname" style="display:none;width:100%" placeholder="Your Comment" rows="2" cols="105" maxlength="200" ></textarea>';
                    Button = '<button type="button" id="Addbtn" onclick="showcomment()" remaning="show" class="btn btn-warning btn-sm" >Add Comments</button>';
                }
                var dropdownA = "";
                var selectAS = "";
                var selectAE = "";
                var selectKS = "";
                var selectKE = "";
                var i = 1;
                if ($(table1).find("allowGuests").text() == "YES") {
                    selectAS += '<select style="margin-left: 260px;display:none;" class="adult"><option value="0">Adults</option>';
                    selectAE += '</select>';
                    selectKS += '<select style="display:none;" class="kids"><option value="0">Kids</option>';
                    selectKE += '</select>';
                    for (i = 1; i <= 20; i++) {
                        dropdownA += '<option value="' + i + '">' + i + '</option>';
                    }

                }
                if ($(table1).find("allowMultiSelection").text() == "YES") {
                    MultiSelection += '<input type="checkbox" onclick="MultiSelection(this)" class="check" ><label class="radio" for=""></label>';
                } else {
                    MultiSelection += '<input type="checkbox" class="check" onclick="Selection(this)"><label class="radio" for=""></label>';
                }
                if (table2.length > 0) {
                    $.each(table2, function (key, value) {
                        var poll = $(this).find("name").text();
                        var atid = $(this).find("AutoId").text();

                        vote += '<div class="col-sm-12 p-3">' + MultiSelection + '&nbsp&nbsp<span class="atid" style="display:none;">' + atid + '</span><span class="ansid" style="display:none;"></span><span class="pollname">' + poll + '</span>' + selectAS + '' + dropdownA + ' ' + selectAE + ' ' + selectKS + '' + dropdownA + '' + selectKE + '' + comment + '</div>';
                    });
                };

                $('#showpoll').html(html);
                $('#buton').html(Button);
                $('#Vote1').html(vote);
                $('#pollidd').html('Poll Id - '+pollid);

            }
        },

        error: function () {
            alert('Show poll Error !!');

        }

    });

}
function showcomment() {
    $("#Vote1 div").each(function () {
        $("#Hidebtn").attr('remaning', "show", true)
        if ($(this).find('input').prop('checked') == true) {
            $(this).find('textarea').show()
            $("#Hidebtn").show();
            $("#Addbtn").hide();
        }
    })
}
function hidecomment() {
    $("#Addbtn").show();
    $("#Hidebtn").hide();
    $(".coment").hide();
}
function copyToClipboard() {
    var element = $("#pollid").val();
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(element).select();
    document.execCommand("copy");

    $temp.remove();
}

function Selection(y) {
    $('.coment').hide();
    $('.kids').hide();
    $('.adult').hide();

    $("#Vote1  div").each(function () {
        if ($(this).find('input').prop('checked') == true) {
            $('.check').prop('checked', false);
            $('.coment').hide();
            $('.kids').hide();
            $('.adult').hide();
            if ($(this).find('input').prop('checked') == false) {
                $(y).prop('checked', true)
                $(this).find("select").show();

                if ($("#Addbtn").css('display') == "none") {
                    $(this).find('textarea').show();
                }

            } else {
                $(this).find('input').prop('checked', false);


            }
        }
    })

}

function MultiSelection(e) {


    $("#Vote1 div").each(function () {
        if ($(this).find('input').prop('checked') == true) {
            $(this).find('.adult').show();
            $(this).find('.kids').show();
            if ($("#Addbtn").css('display') == "none") {
                $(this).find('textarea').show();
            } else {
                $(this).find('textarea').hide();
            }
        } else if ($(this).find('input').prop('checked') == false) {
            $(this).find('.adult').hide();
            $(this).find('.kids').hide();
            $(this).find('textarea').hide();

        }
    })
}


function Votepoll() {
    var check = validateRe();
    if (check) {
        debugger;
        var count = $('#AnswerType .getName');
        if (count.length > 1) {
            var check = "";
            var coment = "";
            var adult = "";
            var adult = "";
            var kids = "";
            let votingdetails = "";
            var voteOption = 0;
            $("#Vote1 div").each(function () {
                if ($(this).find('.check').is(":checked") == true) {
                    votingdetails += "<vote>"
                    votingdetails += "<name>" + ($(this).find('.pollname').text() || '') + "</name>";
                    votingdetails += "<coment>" + ($(this).find('.coment').val() || 0) + "</coment>";
                    if (parseInt($(this).find('.adult  option:selected').val())) {
                        adult = $(this).find('.adult  option:selected').val();
                    } else {
                        adult = 0;
                    }
                    votingdetails += "<adult>" + adult + "</adult>";
                    if (parseInt($(this).find('.kids option:selected').val())) {
                        kids = $(this).find('.kids option:selected').val();
                    } else {
                        kids = 0;
                    }
                    votingdetails += "<kids>" + kids + "</kids>";
                    votingdetails += "<atid>" + ($(this).find('.atid').text() || 0) + "</atid>";
                    votingdetails += "</vote>"
                    voteOption = 1;
                }
            });
            if (voteOption==0) {
                fire_toast("Invalid Poll !, Please select Yes/No.", "error");
                return;
            }
            var data = {
                QuestionAutoId: $("#QueID").val().trim(),
                Email: $("#UserEmail1").val(),
                Name: $("#UserName1").val(),
                votingdetails: votingdetails
            };
            $.ajax({
                url: 'showPoll.aspx/createvote',
                type: 'post',
                data: JSON.stringify({ pobj: data }),
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (e) {
                    if (e.d != '') {
                        debugger;
                        var xmldoc = $.parseXML(e.d)
                        var password = $(xmldoc).find('Table');
                        var pass = '';
                        if ($(password).find("Password").text() != '') {
                            pass = "(Your password is : " + $(password).find("Password").text() + ")";
                        } else {
                            pass = '';
                        }
                        swal({
                            closeOnClickOutside: false,
                            text: "Vote Successfull. " + pass,
                            icon: "success",
                        }).then(function (isConfirm) {
                            location.reload();
                        })
                    } else {
                        if (e.d == 'false') {
                            swal({
                                closeOnClickOutside: false,
                                text: "Vote Failed.",
                                icon: "Error",
                            }).then(function (isConfirm) {
                                location.reload();
                            })
                        } else if (e.d == 'Session Expired') {
                            swal({
                                closeOnClickOutside: false,
                                text: "Session expired",
                            }).then(function (isConfirm) {
                                location.href = '/';
                            })
                        }
                        else if (e.d == 'DuplicatePoll') {
                            swal('', 'Dear ' + $("#UserName1").val()+' you have already poll. If you want to update do login', 'warning');
                        }
                    }
                },
                error: function () {
                }

            });
        }
        else {
            $('#GetAnsType').addClass('border-warning');
            $('#GetAnsType').focus();
            fire_toast("Add minimum two answer type option.", "error");
        }
    }
}
function Displayresult(pid) {
    $.ajax({
        url: 'showPoll.aspx/Displayresult',
        type: 'post',
        data: "{pollid : '" + pid + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",

        success: function (res) {
            debugger;
            if (res != "") {
                var xmldoc = $.parseXML(res.d)
                var result = $(xmldoc).find('Table');
                var anstype = $(xmldoc).find('Table1');
                var name = '';
                var username = "";
                var Comments = "";
                var Adult = '';
                var Kid = '';
                var display = '';
                if (anstype.length > 0) {
                    var countresult = 0;

                    $.each(anstype, function (key, value) {
                        debugger;

                        var text = $(this).find("name").text();
                        if (parseInt($(this).find("legaluser").text()) == 0 && $(this).find("DisplayResults").text() == "true") {
                            display = "Aok";
                        } else if (parseInt($(this).find("legaluser").text()) == 1) {
                            display = "ok";
                        }
                        countresult = 0;
                        var Kids = 0;
                        var Adults = 0;
                        var BO = '';
                        var BM = '';
                        var BC = '';
                        $.each(result, function (key, value1) {
                            if ($(value1).find("chechbox").text() == text) {
                                debugger;
                                countresult++;
                                username = '<tr><td style="color:black;width: 50%;">' + $(this).find("Name").text();

                                if ($(this).find("Kids").text() != 0 || $(this).find("Adults").text() != 0) {
                                    BO = ' (';
                                    BM = ' + ';
                                    BC = ')';
                                    Adults = Adults + Number($(this).find("Adults").text());
                                    Kids = Kids + Number($(this).find("Kids").text());
                                    Adult = ' (' + $(this).find("Adults").text();
                                    Kid = ' + ' + $(this).find("Kids").text() + ')';
                                } else {
                                    Kid = '';
                                    Adult = '';
                                }
                                username += Adult + Kid + '</td>';
                                if ($(this).find("Comments").text() != 0) {
                                    Comments += username + '<td style="color:black;width: 50%;">' + $(this).find("Comments").text() + '</td></tr>';

                                } else {
                                    Comments += username + '<td style="color:black;width: 50%;"></td></tr>';
                                }
                            } else {
                                //countresult = '';
                                //kids = '';
                                //adults = '';
                            }
                        });

                        var poll1 = $(this).find("name").text();
                        if (display == "Aok") {
                            name += '<div class="pollresult"><div class="card-header mb-0" style="background: #f3d2c8 !important;border: 1px solid #f3d2c8;"><h4 class="card-title">' + poll1 + ' (' + countresult + ')' + BO + '' + (Adults == 0 ? "" : Adults) + '' + BM + '' + (Kids == 0 ? "" : Kids) + '' + BC + '' + '</h4></div>';

                            name += '<div><table class="table table-bordered" style="width: 100%;"><tbody>' + Comments + '</tbody></table></div></div>';
                        } else if (display == "ok") {
                            name += '<div class="pollresult"><div class="card-header mb-0" style="background: #f3d2c8 !important;border: 1px solid #f3d2c8;"><h4 class="card-title">' + poll1 + ' (' + countresult + ')' + BO + '' + (Adults == 0 ? "" : Adults) + '' + BM + '' + (Kids == 0 ? "" : Kids) + '' + BC + '' + '</h4></div>';

                            name += '<div><table class="table table-bordered" style="width: 100%;"><tbody>' + Comments + '</tbody></table></div></div>';
                        }
                        username = '';
                        Comments = '';
                        //adults = '';
                        //Kids = "";
                    });
                }
                console.log(name);
                $('#comment').html(name);
            }
        },
        error: function (xhr, status, error) {
            alert(error.responseTextss);
        }
    })
}
function showmodal() {
    debugger;
    $("#CreatePoll").modal('show');
    //$("#CreatePollCenterTitle").hide();
    $("#CreatePollCenterTitl").text('Update Poll');
    $("#r3").prop('checked', false);
    $("#r1").prop('checked', false);
    $("#showans").show();
    $("#savebtn").hide();
    $("#clcgenbox").hide();
    $("#clcupdbox").show();
    $("#updatebtn").show();
    var Que = $('#QueID').val().trim()
    $.ajax({
        url: 'showPoll.aspx/editquestion',
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        data: "{QuestionAutoId: " + Que + "}",
        success: function (ds) {
            debugger;
            $("#showans").show();
            if (ds != "") {
                var xmldoc = $.parseXML(ds.d)
                var Question = $(xmldoc).find('Table');
                var Quetype = $(xmldoc).find('Table1');
                $("#UserQust").val($(Question).find("Question").text());
                var ans = '';
                if ($(Question).find("AllowComments").text() == "YES") {
                    $("#r3").prop('checked', true);
                }
                if ($(Question).find("allowGuests").text() == "YES") {
                    $("#r2").prop('checked', true);
                }
                if ($(Question).find("allowMultiSelection").text() == "YES") {
                    $("#r4").prop('checked', true);
                }
                if ($(Question).find("DisplayResults").text() == "YES") {
                    $("#r1").prop('checked', true);
                }
                $.each(Quetype, function (key, value) {
                    debugger;
                    var name = $(this).find("name").text();
                    var atid = $(this).find("AutoId").text();
                    ans += '<div class="input-group mb-2"><div class="input-group-text change_cls bgcolor" onclick = "RemoveTextBox(this)" ><i class="fa fa-minus" style="font-size: 19px; color: #fff;"></i></div ><input type="text" class="form-control req getName" value="' + name + '" style="text-transform: uppercase;" placeholder="ADD OPTION "><span class="attid" style="display:none;">' + atid + '</span></div>'
                });

                $('#showans').html(ans);
            }
        }

    });
}

function updatePolls() {
    var Que = $('#QueID').val().trim()
    var AnsTypesNames = '';
    $("#showans .mb-2").each(function () {
        debugger;
        if ($(this).find('.getName').val().trim() != '') {
            AnsTypesNames += "<vote>";
            AnsTypesNames += "<name>" + $(this).find('.getName').val().trim() + "</name>";
            if (parseInt($(this).find('.attid').text())) {
                atid = $(this).find('.attid').text();
            } else {
                atid = 0
            }
            AnsTypesNames += "<atid>" + atid + "</atid>";
            AnsTypesNames += "</vote>"
        }
    });

    var data = {
        Question: $('#UserQust').val().trim(),
        votingdetails: AnsTypesNames,
        DisplayResults: $('#r1').is(":checked"),
        allowGuests: $('#r2').is(":checked"),
        AllowComments: $('#r3').is(":checked"),
        allowMultiSelection: $('#r4').is(":checked"),
        QuestionAutoId: Que
    };
    if ($("#UserQust").val().trim() != '') {
        $.ajax({
            url: 'showPoll.aspx/updatePoll',
            type: 'post',
            data: JSON.stringify({ pobj: data }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (e) {
                debugger;
                if (e.d == "session expired") {
                    swal({
                        closeOnClickOutside: false,
                        text: "Session expired! Please Log In",
                    }).then(function (isConfirm) {
                        location.reload();
                    })
                } else {
                    if (e.d == "vote updated") {
                        Displayresult();
                        swal({
                            closeOnClickOutside: false,
                            text: "Poll Updated Successfully.",
                            icon: "success",
                        }).then(function (isConfirm) {
                            location.reload();
                        })
                    } else {
                        if (e.d == 'voted updated') {
                            swal({
                                closeOnClickOutside: false,
                                text: "Vote update Failed.",
                                icon: "Error",
                            }).then(function (isConfirm) {

                            })
                        } else if (e.d == 'Session Expired') {
                            swal({
                                closeOnClickOutside: false,
                                text: "Session expired",
                            }).then(function (isConfirm) {
                                location.href = '/';
                            })
                        }
                    }
                }
            }
        })
    } else {
        $('#UserQust').addClass('border-warning');
        $('#UserQust').focus();
        fire_toast("All * fields are mandatory.", "error");
        return false;
    }
    
}
function getpolldetails() {
    var Que = $('#QueID').val().trim()
    $.ajax({
        url: 'showPoll.aspx/getpolldet',
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        data: "{QuestionAutoId: " + Que + "}",
        success: function (ds) {
            debugger;
            if (ds.d != '') {
                var xmldoc = $.parseXML(ds.d)
                var votedet = $(xmldoc).find('Table');
                $.each(votedet, function (key, value) {
                    debugger;
                    $("#Vote1 div.p-3").each(function () {
                        debugger;
                        $('#updatevote').show();
                        $('#submitvote').hide();
                        if ($(value).find("Name").text() == $(this).find(".pollname").text()) {
                            console.log($(value).find("Comments").text());
                            $(this).find(".check").prop('checked', true);
                            $(this).find(".coment").show();
                            if ($(value).find("Comments").text() != 0) {
                                var com = $(value).find("Comments").text();
                            }
                            $(this).find(".coment").val(com);
                            $(this).find(".kids").show();
                            $(this).find(".adult").show();
                            $(this).find(".adult").val($(value).find("Adults").text());
                            $(this).find(".kids").val($(value).find("Kids").text());
                            $(this).find(".ansid").text($(value).find("AutoId").text());
                        }

                    });
                });
            }
        }
    })
}
function updateVotepoll() {
    debugger;
    var votingdetails = '';
    $("#Vote1 div.p-3").each(function () {
        debugger;
        if ($(this).find('.check').is(":checked") == true) {
            votingdetails += "<vote>"
            votingdetails += "<name>" + ($(this).find('.pollname').text() || '') + "</name>";
            votingdetails += "<coment>" + $(this).find('.coment').val() + "</coment>";
            if (parseInt($(this).find('.adult  option:selected').val())) {
                adult = $(this).find('.adult  option:selected').val();
            } else {
                adult = 0;
            }
            votingdetails += "<adult>" + adult + "</adult>";
            if (parseInt($(this).find('.kids option:selected').val())) {
                kids = $(this).find('.kids option:selected').val();
            } else {
                kids = 0;
            }
            votingdetails += "<kids>" + kids + "</kids>";
            votingdetails += "<atid>" + ($(this).find('.atid').text() || 0) + "</atid>";
            votingdetails += "</vote>"
        }
    });
    var data = {
        QuestionAutoId: $("#QueID").val().trim(),
        votingdetails: votingdetails
    };
    $.ajax({
        url: 'showPoll.aspx/updateVote',
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ pobj: data }),
        success: function (e) {
            debugger;
            if (e.d == "voted") {
                swal({
                    closeOnClickOutside: false,
                    text: "Vote Updated Successfully.",
                    icon: "success",
                }).then(function (isConfirm) {
                    location.reload();
                })
            } else {
                if (e.d == 'false') {
                    swal({
                        closeOnClickOutside: false,
                        text: "Vote Failed.",
                        icon: "Error",
                    }).then(function (isConfirm) {
                        location.reload();
                    })
                } else if (e.d == 'Session Expired') {
                    swal({
                        closeOnClickOutside: false,
                        text: "Session expired",
                    }).then(function (isConfirm) {
                        location.href = '/';
                    })
                }
            }
        }
    });
}
function Changests() {
    var text = '';
    var status = $("#status").text();
    if (status == "Open") {
        text = "You want to Close Poll."
    } else {
        text = "You want to Reopen Poll."

    }
    swal({
        title: "Are you sure?",
        closeOnClickOutside: false,
        text: text,
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            Change();
        }
    })
}

function Change() {
    var Pid = $('#polid').val().trim();
    console.log(Pid);
    debugger
    var data = {
        pollid: Pid
    };
    $.ajax({
        url: 'showPoll.aspx/Changests',
        type: 'post',
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ pobj: data }),
        success: function (e) {
            debugger;
            if (e.d == "changed") {
                swal({
                    closeOnClickOutside: false,
                    text: "Status Updated Successfull.",
                    icon: "success",
                }).then(function (isConfirm) {
                    location.reload();
                })
            } else {
                if (e.d == 'false') {
                    swal({
                        closeOnClickOutside: false,
                        text: "Status update Failed.",
                        icon: "Error",
                    }).then(function (isConfirm) {
                        location.reload();
                    })
                } else if (e.d == 'Session Expired') {
                    swal({
                        closeOnClickOutside: false,
                        text: "Session expired",
                    }).then(function (isConfirm) {
                        location.href = '/';
                    })
                }
            }
        }
    });
}

