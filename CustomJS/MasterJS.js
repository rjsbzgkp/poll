﻿$(document).ready(function () {
    $('#UserName').keyup(function () {
        if ($('#UserName').val() == '') {
            $('#UserName').addClass('border-warning');
        } else {
            $('#UserName').removeClass('border-warning');
        }
    })
    $('#UserEmail').keyup(function () {
        if ($('#UserEmail').val() == '') {
            $('#UserEmail').addClass('border-warning');
        } else {
            $('#UserEmail').removeClass('border-warning');
        }
    })
    $('#UserQust').keyup(function () {
        if ($('#UserQust').val() == '') {
            $('#UserQust').addClass('border-warning');
        } else {
            $('#UserQust').removeClass('border-warning');
        }
    })
})

function signinmodalfun() {
    $('#CreatePoll').modal('hide');
    $('#signinmodal').modal('show');
}

function validateReg() {

    var Questions = $('#UserQust').val().trim();
    if (session != 'yes') {
        var name = $('#UserName').val().trim();
        var email = $('#UserEmail').val().trim().toLowerCase();
        if (name == null || name == "") {
            $('#UserName').addClass('border-warning');
            $('#UserName').focus();
            fire_toast("All * fields are mandatory.", "error");
            return false;
        }
        else {
            $('#UserName').removeClass('border-warning');
        }
        if (email == null || email == "") {
            $('#UserEmail').addClass('border-warning');
            $('#UserEmail').focus();
            fire_toast("All * fields are mandatory.", "error");
            return false;
        }
        else {
            var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (email.match(mailformat)) {
                $('#UserEmail').removeClass('border-warning');
            }
            else {
                $('#UserEmail').addClass('border-warning');
                $('#UserEmail').focus();
                fire_toast("Please enter valid Email ID.", "error");
                return false;
            }
        }
    }
    if (Questions == null || Questions == "") {
        $('#UserQust').addClass('border-warning');
        $('#UserQust').focus();
        fire_toast("All * fields are mandatory.", "error");
        return false;
    }
    else {
        $('#UserQust').removeClass('border-warning');
    }
    return true;
}

function GenerateTextBox() {
    $('#GetAnsType').removeClass('border-warning');
    $('#clcgenbox').css('background', '#f1f5fa');
    var d = $('#GetAnsType').val().trim();
    if (d != '') {
        var data = '';
        data += '<div class="input-group mb-2"><div onclick="RemoveTextBox(this)" class="input-group-text change_cls bgcolor" ><i class="fa fa-minus" style="font-size: 19px;color: #fff;"></i></div><input type="text" class="form-control req getName" value="' + d + '" style="text-transform: uppercase" placeholder="ADD OPTION *"></div>';
        $('#AnswerType').append(data); 
        $('#GetAnsType').val('');
        $('#GetAnsType').removeClass('border-warning');
    }
    else {
        $('#GetAnsType').addClass('border-warning');
        $('#GetAnsType').focus();
        fire_toast("Please enter the option.", "error");
    }
}

function RemoveTextBox(e) {
    $(e).closest('.input-group').remove();
}
function GenerateUpdTextBox() {
    $('#GetAnsType').removeClass('border-warning');
    $('#clcgenbox').css('background', '#f1f5fa');
    var d = $('#GetAnsType').val().trim();
    if (d != '') {
        var data = '';
        data += '<div class="input-group mb-2"><div onclick="RemoveTextBox(this)" class="input-group-text change_cls bgcolor" ><i class="fa fa-minus" style="font-size: 19px;color: #fff;"></i></div><input type="text" class="form-control req getName" value="' + d + '" style="text-transform: uppercase" placeholder="ADD OPTION *"></div>';
        $('#showans').append(data);
        $('#GetAnsType').val('');
        $('#GetAnsType').removeClass('border-warning');
    }
    else {
        $('#GetAnsType').addClass('border-warning');
        $('#GetAnsType').focus();
        fire_toast("Please enter the option.", "error");
    }
}
function show1() {
    $("#AnswerType").show();
    $("#clcupdbox").hide();
    $("#clcgenbox").show();
    $("#r3").prop('checked', true);
    $("#r1").prop('checked', true);
    $("#savebtn").show();
}
function close1() {
    $("#CreatePollCenterTitl").text('Create Poll');
    $("#AnswerType").hide();
    $("#UserQust").val('');
    $("#showans").hide();
    $('#AnswerType').hide();
    $("#r1").prop('checked', false);
    $("#r2").prop('checked', false);
    $("#r3").prop('checked', false);
    $("#r4").prop('checked', false);
    $("#updatebtn").hide();
}

function CreatePolls() {
    debugger
    var checkreq = validateReg();
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    var result = ""
    var chaactersLength = characters.length;

    for (var i = 0; i < 8; i++) {
        result += characters.charAt(Math.floor(Math.random() * chaactersLength));
    }

    if (checkreq == true) {
        var count = $('#AnswerType .getName');
        if (count.length > 1) {
            $('#GetAnsType').removeClass('border-warning');
            var counter = 0;
            var AnsTypesNames = '';
            $('#AnswerType .getName').each(function () {
                var AnsTypesName = $(this).val().trim().toUpperCase();
                if (AnsTypesName == '') {
                    $(this).addClass('border-warning');
                    $(this).focus();
                    counter++;
                }
                else {
                    $(this).removeClass('border-warning');
                }
                AnsTypesNames += AnsTypesName + ',';
            });
            if (counter == 0) {
                if ($('#GetAnsType').val().trim() == '') {
                    $('#clcgenbox').css('background', '#f1f5fa');
                    var uname = ''; var uemails = '';
                    if (session != 'yes') {
                        uname = $('#UserName').val().trim().toUpperCase();
                        uemails = $('#UserEmail').val().trim().toUpperCase();
                    }
                    else {
                        uname = '';
                        uemails = '';
                    }
                    var data = {
                        Name: uname,
                        Email: uemails,
                        pollid: result,
                        Question: $('#UserQust').val().trim(),
                        AnsTypeName: AnsTypesNames,
                        DisplayResults: $('#r1').is(":checked"),
                        allowGuests: $('#r2').is(":checked"),
                        AllowComments: $('#r3').is(":checked"),
                        allowMultiSelection: $('#r4').is(":checked"),
                    };
                    $.ajax({
                        type: "POST",
                        url: "/WebApi/MasterPage.asmx/CreatesPoll",
                        data: JSON.stringify({ pobj: data }),
                        contentType: "application/json; charset=utf-8",
                        datatype: "json",
                        beforeSend: function () {

                        },
                        complete: function () {

                        },
                        success: function (response) {
                            if (response.d != 'Exists') {
                                var XML1 = $.parseXML(response.d);
                                var datas1 = $(XML1).find("Table");
                                var pass = '';
                                var id = '';
                                $.each(datas1, function () {
                                    pass = $(this).find("Pass").text();
                                    id = $(this).find("AutoId").text();
                                });
                                if (response.d == "false") {
                                    location.href = '/';
                                }
                                else {
                                    if (pass != '') {
                                        swal({
                                            title: "Poll created successfully.",
                                            text: "Your password is : " + pass,
                                            icon: "success",
                                         closeOnClickOutside: false
                                        }).then(function () {
                                            location.href = '/PollList.aspx';
                                        });
                                    }
                                    else {
                                        swal({
                                            closeOnClickOutside: false,
                                            text: "Poll created successfully.",
                                            icon: "success",
                                        }).then(function (isConfirm) {
                                            location.href = '/PollList.aspx';
                                        })
                                    }
                                }
                            }
                            else {
                                swal({
                                    closeOnClickOutside: false,
                                    text: "This Email Id is already exists. Please sign in to create a poll.",
                                    icon: "error",
                                }).then(function (isConfirm) {
                                 //   signinmodalfun();
                                })                               
                            }
                        },
                        error: function (result) {
                            swal("", "Error in syntax", "error");
                        },
                        failure: function (result) {
                            swal("", 'Oops,some thing  wrong.Please try again.', "error");
                        }
                    });
                }
                else {
                    $('#clcgenbox').css('background', 'red');
                    fire_toast("Please add the option.", "error");
                }
            }
            else {
                fire_toast("All * fields are mandatory.", "error");
            }
        }
        else {
            $('#GetAnsType').addClass('border-warning');
            $('#GetAnsType').focus();
            fire_toast("Add minimum two answer type option.", "error");
        }
    }
}

function Login() {
    if ($('#UserEmailId').val().trim() != '') {
        var data = {
            Email: $('#UserEmailId').val().trim(),
            Password: $('#UserPass').val().trim(),
        };
        $.ajax({
            type: "POST",
            url: "/WebApi/MasterPage.asmx/Login",
            data: JSON.stringify({ pobj: data }),
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (response) {
                if (response.d == "no record") {
                    swal({
                        closeOnClickOutside: false,
                        text: "Incorrect Email or Password",
                        icon: "error",
                    })
                }
                else {
                    location.href = '/PollList.aspx';
                }
            },
            error: function (result) {
                swal("", "Error in syntax", "error");
            },
            failure: function (result) {
                swal("", 'Oops,some thing  wrong.Please try again.', "error");
            }
        });
    }
    else {
        $('#UserEmailId').addClass('border-warning');
        fire_toast("All * fields are mandatory.", "error");
    }
}

function Logoutclick() {
    swal({
        title: "Are you sure?",
        closeOnClickOutside: false,
        text: "You want to logout.",
        icon: "warning",
        showCancelButton: true,
        buttons: {
            cancel: {
                text: "Cancel",
                value: null,
                visible: true,
                className: "btn-warning",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    }).then(function (isConfirm) {
        if (isConfirm) {
            Logout();
        }
    })
}

function Logout() {
    window.location.href = "/";
    $.ajax({
        type: "POST",
        url: "/WebApi/MasterPage.asmx/Logout",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
        },
        error: function (result) {
            console.log(result);
        },
        failure: function (result) {
            console.log(JSON.parse(result.responseText).d);
        }
    });
}
function forgotPassword() {
    $('#signinmodalCenterTitle').html('Forgot password');
    $('#divPsw').hide();
    $('#btnSubmitEmail').show();
    $('#divForgotPassMessage').show();
    $('#btnClose').hide();
    $('#btnLogin').hide();
    $('#btnForPassword').hide();
    $('#UserEmailId').val($('#UserEmail').val());
}

function SendConfirmEmail() {
    if ($('#UserEmailId').val().trim() == '') {
        swal("", "Email address is required.", "error");
        return;
    }
    $.ajax({
        type: "POST",
        url: "/webapi/MasterPage.asmx/ForgotPassword",
        data: "{Email:'" + $('#UserEmailId').val().trim() + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: "false",
        beforeSend: function () {
            $('#fade').show();
        },
        complete: function () {
            $('#fade').hide();
        },
        success: function (data) {
            if (data.d == 'success') {
                swal({
                    title: "",
                    text: "Reset Password link has been sent on your registered email.",
                    icon: "success",
                    closeOnClickOutside: false,
                    closeOnEsc: false
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $('#UserEmailId').val('');
                        window.location.reload();
                    }
                });
            }
            else {
                swal("", data.d, "error");
            }
        },
        error: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        },
        failure: function (result) {
            swal("Error!", "Oops! Something went wrong.Please try later.", "error");
        }
    });
}