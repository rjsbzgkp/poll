﻿var emailid = "",UserId=0;
$(document).ready(function () {
    debugger
    var getQueryString = function (field, url) {
        var href = url ? url : window.location.href;
        var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
        var string = reg.exec(href);
        return string ? string[1] : null;
    };
    UserId = getQueryString('asdfgh');
    verifyUser(UserId);
});
function verifyUser(UserId) {
    $.ajax({
        url: "/webapi/MasterPage.asmx/VerifyUser",
        type: 'post',
        data: "{AutoId:'" + UserId + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (e) {
            if (e.d == "PasswordChanged") {
                swal({
                    closeOnClickOutside: false,
                    text: "Password has been already changed.",
                    icon: "error",
                }).then(function (isConfirm) {
                    window.location.href = "/Default.aspx";
                })
            }
            else if (e.d == "UnauthorizesAccess") {
                swal({
                    closeOnClickOutside: false,
                    text: "Unauthorizes Access !!.",
                    icon: "error",
                }).then(function (isConfirm) {
                    window.location.href = "/Default.aspx";
                })
            }
            else if (e.d == "true") {
                $('#divMainRP').show();
            }
        },
    });
}
function ChangePassword() {
    debugger
    if ($('#txtPassword').val().trim() =="" &&  $('#txtConfirmPassowrd').val().trim()=="") {
        fire_toast("New Password and Confirm Password is required.", "error");
        return;
    }
    else if ($('#txtPassword').val().trim() != $('#txtConfirmPassowrd').val().trim()) {
        fire_toast("Password and Confirm Password does not match.", "error");
        return;
    }
    $.ajax({
        url: "/webapi/MasterPage.asmx/ResetPassword",
        type: 'post',
        data: "{AutoId:'" + UserId + "',Password:'" + $('#txtPassword').val().trim() + "'}",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (e) {
            if (e.d == 'success') {
                swal({
                    closeOnClickOutside: false,
                    text: "Password has been changed successfully. ",
                    icon: "success",
                }).then(function (isConfirm) {
                    $('#divMainRP').hide();
                    signinmodalfun();
                })
            } else {
                swal({
                    closeOnClickOutside: false,
                    text: e.d,
                    icon: "Error",
                })
            }
        },
        error: function () {
        }
    });
}