﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterDLL;

public partial class showPoll : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/CustomJS/showPoll.js"));
        Page.Header.Controls.Add(
             new System.Web.UI.LiteralControl(
                "<script>" + text + "</script>"
            ));

    }
    [WebMethod(EnableSession = true)]
    public static string showpoll(string pollid)
    {

        PL_CreartPoll pobj = new PL_CreartPoll();
        pobj.pollid = pollid;
        if (HttpContext.Current.Session["AutoId"] != null)
        {
            pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

        }
        BAL_CreartPoll.Showpoll(pobj);
        if (!pobj.isException)
        {
            return pobj.Ds.GetXml();
        }
        else
        {
            return "false";
        }

    }

    [WebMethod(EnableSession = true)]
    public static string createvote(PL_CreartPoll pobj)
    {
        try
        {
            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);
            }
            else
            {
                pobj.Password = Utility.password();
            }

            BAL_CreartPoll.createvote(pobj);
            if (!pobj.isException)
            {
                if (HttpContext.Current.Session["AutoId"] == null)
                {
                    HttpContext.Current.Session.Add("AutoId", pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString());
                    HttpContext.Current.Session.Add("Name", pobj.Name);
                }
                return pobj.Ds.GetXml();
            }
            else
            {
                if (pobj.exceptionMessage == "DuplicatePoll")
                {
                    return "DuplicatePoll";
                }
                else
                {
                    return "false";
                }
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }
    [WebMethod(EnableSession = true)]
    public static string Displayresult(string pollid)
    {

        try
        {
            PL_CreartPoll pobj = new PL_CreartPoll();
            pobj.pollid = pollid;

            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

            }
            BAL_CreartPoll.Displayresult(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }

    [WebMethod(EnableSession = true)]


    public static string editquestion(int QuestionAutoId)
    {

        try
        {
            PL_CreartPoll pobj = new PL_CreartPoll();
            pobj.QuestionAutoId = Convert.ToInt32(QuestionAutoId);

            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

            }
            BAL_CreartPoll.editquestion(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }

    [WebMethod(EnableSession = true)]


    public static string updatePoll(PL_CreartPoll pobj)
    {

        try
        {
            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

            }
            BAL_CreartPoll.updatePoll(pobj);
            if (!pobj.isException)
            {
                return "vote updated";
            }
            else
            {
                return "Vote Failed";
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }

    [WebMethod(EnableSession = true)]


    public static string getpolldet(int QuestionAutoId)
    {

        try
        {
            PL_CreartPoll pobj = new PL_CreartPoll();
            pobj.QuestionAutoId = Convert.ToInt32(QuestionAutoId);
            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

            }
            BAL_CreartPoll.getpolldet(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "no data";
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }

    [WebMethod(EnableSession = true)]


    public static string updateVote(PL_CreartPoll pobj)
    {

        try
        {
            if (HttpContext.Current.Session["AutoId"] == null && string.IsNullOrEmpty(pobj.Email))
            {
                return "session expired";
            }
            else
            {
                if (HttpContext.Current.Session["AutoId"] != null)
                {
                    pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

                }
                BAL_CreartPoll.updateVote(pobj);
                if (!pobj.isException)
                {
                    return "voted";
                }
                else
                {
                    return "false";
                }
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }
    }

    [WebMethod(EnableSession = true)]


    public static string Changests(PL_CreartPoll pobj)
    {

        try
        {
            if (HttpContext.Current.Session["AutoId"] != null)
            {
                pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"]);

            }
            BAL_CreartPoll.Changests(pobj);
            if (!pobj.isException)
            {
                return "changed";
            }
            else
            {
                return "false";
            }
        }
        catch (Exception ex)
        {
            return "Session Expired";
        }


    }
}