function fire_toast(message, type)
{
	var toastCount = 0;
    var shortCutFunction = type;
    var msg = message;
    var title = $('#title').val() || '';
    var $showDuration = "300";
    var $hideDuration = "2500";
    var $timeOut = "2500";
    var $extendedTimeOut = "2500";
    var $showEasing = "swing";
    var $hideEasing = "linear"
    var $showMethod = "fadeIn";
    var $hideMethod = "fadeOut";
    var toastIndex = toastCount++;

    toastr.options = {
        closeButton: $('#closeButton').prop('checked'),
        debug: $('#debugInfo').prop('checked'),
        newestOnTop: $('#newestOnTop').prop('checked'),
        progressBar: $('#progressBar').prop('checked', true),
        positionClass: $('#positionGroup input:radio:checked').val() || 'toast-top-center',
        preventDuplicates: $('#preventDuplicates').prop('checked'),
        onclick: null
    };

    if ($showDuration.length) {
        toastr.options.showDuration = $showDuration;
    }
    if ($hideDuration.length) {
        toastr.options.hideDuration = $hideDuration;
    }
    if ($timeOut.length) {
        toastr.options.timeOut = $timeOut;
    }
    if ($extendedTimeOut.length) {
        toastr.options.extendedTimeOut = $extendedTimeOut;
    }
    if ($showEasing.length) {
        toastr.options.showEasing = $showEasing;
    }
    if ($hideEasing.length) {
        toastr.options.hideEasing = $hideEasing;
    }
    if ($showMethod.length) {
        toastr.options.showMethod = $showMethod;
    }
    if ($hideMethod.length) {
        toastr.options.hideMethod = $hideMethod;
	}
    var $toast = toastr[shortCutFunction](msg, title);	
}