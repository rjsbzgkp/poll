﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToInt32(System.Web.HttpContext.Current.Session["AutoId"]) > 0)
        {
            Label1.Text = Session["Name"].ToString();
        }
        

        string script = File.ReadAllText(Server.MapPath("/CustomJS/MasterJS.js"));
        Page.Header.Controls.Add(
             new System.Web.UI.LiteralControl(
                "<script>" + script + "</script>"
            ));

        string text = File.ReadAllText(Server.MapPath("/assets/css/CustomCss.css"));
        Page.Header.Controls.Add(
             new System.Web.UI.LiteralControl(
                "<style>" + text + "</style>"
            ));
    }
}
