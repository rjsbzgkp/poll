﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="showPoll.aspx.cs" Inherits="showPoll" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
         @media only screen and (max-width:991px) {
            #shead .col-md-6{text-align:center!important}             
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="card-header">
        <div id="shead" class="row align-items-center">
            <div class="col-md-6 mb-1"><h4 class="card-title" style="color:white;font-size:1.5rem">   <span id="pollidd" style="position: relative;top: 2px;float: left;"></span></h4></div>
            <div class="col-md-6 mb-1">
                 <%if (HttpContext.Current.Session["AutoId"] != null)
                        {%>
                    <span style="float: right;">
                        <button type="button" style="color:black;display:none;margin:0 5px" id="chngsts" onclick="Changests()" class="btn btn-warning"><i class="fa fa-edit"></i>  Change Status</button></span>
                    <span style="float: right;">
                        <button type="button" style="display:none;" id="modaledit" onclick="showmodal()" class="btn btn-primary"><i class="fa fa-edit"></i>  Edit Poll</button></span>
                    <% } %>
            </div>
            <div class="col">

                <%--<h4 class="card-title" style="color:white;font-size:1.5rem">--%>                   
                   <%-- <span id="pollidd" style="position: relative;top: 9px;"></span>--%>
                   
                <%--</h4>--%>
            </div>
        </div>
    </div>
    <div class="table-responsive browser_users" id="showpoll">
    </div>
    <div class="table-responsive browser_users" id="datapoll">
    </div>
   
    <div class="card-header mb-2">
        <div class="row align-items-center" id="Results">
            <div class="col">
                <h4 class="card-title" style="color:white;font-size:1.5rem">Poll Results </h4>
            </div>
        </div>
    </div>
<%--     <%if (HttpContext.Current.Session["AutoId"] != null)
        {%>--%>
    <div style="font-size:1rem" class="table-responsive browser_users" id="comment">
    </div>
    <%--<% } %>--%>

    <div class="modal fade" id="Vote" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" role="dialog" aria-labelledby="CreatePollCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title m-0" id="CreatePollCenterTitle" style="color:white;font-size:1rem">Your Vote</h6>
                    <button type="button" class="btn-close btnn" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <!--end modal-header-->
                <div class="modal-body">
                    <input type="hidden" id="QueID">
                    <%if (HttpContext.Current.Session["AutoId"] == null)
                        {%>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label labal">Enter Name <span style="color: red;">*</span></label>
                        <div class="col-sm-7">
                            <input class="form-control req" type="text" placeholder="Enter Name " id="UserName1">
                        </div>
                        <div class="col-sm-1 or_class">
                            <span>OR</span>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" onclick="signinmodalfun()" class="btn btn-primary">Sign In</button>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label labal">Enter Email <%--<span style="color: red;">*</span>--%></label>
                        <div class="col-sm-10">
                            <input class="form-control req" type="text" placeholder="Enter Email " id="UserEmail1">
                        </div>
                    </div>
                    <script>
                        var session = 'no';
                    </script>
                    <%}
                        else
                        {%>
                    <script>
                        var session = 'yes';
                    </script>
                    <%} %>
                    <div class="form-input" id="Vote1">
                    </div>
                    <p id="buton"></p>
                    <button type="button" style="display: none;" id="Hidebtn" onclick="hidecomment()" class="btn btn-warning btn-sm">Hide Comments</button>
                </div>
                <!--end modal-body-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal">Close</button>
                    <button type="button" onclick="Votepoll()" id="submitvote" class="btn btn-success btn-sm">Submit</button>
                    <button type="button" onclick="updateVotepoll()" style="display: none;" id="updatevote" class="btn btn-success btn-sm">Submit</button>
                </div>
                <!--end modal-footer-->
            </div>
            <!--end modal-content-->
        </div>
        <!--end modal-dialog-->
    </div>
</asp:Content>


