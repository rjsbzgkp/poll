﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="PollList.aspx.cs" Inherits="PollList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="card-title">My Poll</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body p-2">
                    <div class="row my-0 mx-0 p-0">
                        <div class="col-md-2 mt-1">
                            <select id="status" class="form-control p-2" required="">
                                <option value="" selected="">ALL</option>
                                <option value="1">Open</option>
                                <option value="0">Close</option>
                            </select>
                        </div>
                        <div class="col-md-3 mt-1">
                            <div class="input-group mb-0 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">From</div>
                                </div>
                                <input type="date" class="form-control" name="to_date" id="fromDate" placeholder="yyyy-mm-dd" value="">
                            </div>
                        </div>
                        <div class="col-md-3 mt-1">
                            <div class="input-group mb-0 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">To</div>
                                </div>
                                <input type="date" class="form-control" name="to_date" id="toDate" placeholder="yyyy-mm-dd" value="">
                            </div>
                        </div>
                        <div class="col-md-3 mt-1">
                            <button type="button" onclick="getfilterdata()" class="btn btn-success btn-md p-2">Search</button>
                             
                            <span class="poll" id="countQue">0</span>
                        </div>
                        <%--<div class="col-md-1 mt-0 poll">
                            <label style="margin-left: -1px;">Total Poll-</label>
                            <span id="countQue" class="">0</span>
                        </div>--%>

                    </div>
                </div>
                <!--end col-->

            </div>
        </div>
        <!--end card-header-->
        <%--<div class="card">--%>
            <div class="table-responsive browser_users" id="Polllist">

                <%--<table class="table mb-0 text-center">
                    <thead class="thead-light">
                        <tr>
                            <th class="border-top-0" style="text-align: left">Question</th>
                            <th class="border-top-0" style="width: 20%">Created By</th>
                            <th class="border-top-0" style="width: 12%">Created Date</th>
                            <th class="border-top-0" style="width: 12%">Status</th>
                        </tr>
                        <!--end tr-->
                    </thead>
                    <tbody id="allQueList">
                    </tbody>
                </table>--%>
                <!--end table-->
            </div>
            <!--end /div-->
        <%--</div>--%>
        <!--end card-body-->
    </div>
    <!--end card-->
    <%--</div>--%>
    <!--end col-->
    </div>

</asp:Content>

