﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MasterDLL
/// </summary>
namespace MasterDLL
{
    public class PL_CreartPoll : Utility
    {
        public int AutoId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Question { get; set; }
        public string AnsTypeName { get; set; }
        public bool DisplayResults { get; set; }
        public bool AllowComments { get; set; }
        public bool allowGuests { get; set; }
        public bool allowMultiSelection { get; set; }
        public bool Status { get; set; }
        public string votingdetails { get; set; }
        public string pollstatus { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }

        public string pollid { get; set; }

        public int QuestionAutoId { get; set; }
    }
    public class DAL_CreartPoll
    {
        public static void ReturnTable(PL_CreartPoll pobj)
        {
            try
            {
                Config connect = new Config();
                SqlCommand sqlCmd = new SqlCommand("ProcCreatePoll", connect.con);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("@Opcode", pobj.Opcode);
                sqlCmd.Parameters.AddWithValue("@AutoId", pobj.AutoId);
                sqlCmd.Parameters.AddWithValue("@Pollid", pobj.pollid);
                sqlCmd.Parameters.AddWithValue("@Name", pobj.Name);
                sqlCmd.Parameters.AddWithValue("@Email", pobj.Email);
                sqlCmd.Parameters.AddWithValue("@Password", pobj.Password);
                sqlCmd.Parameters.AddWithValue("@Question", pobj.Question);
                sqlCmd.Parameters.AddWithValue("@votingdetails", pobj.votingdetails);
                sqlCmd.Parameters.AddWithValue("@QuestionAutoId", pobj.QuestionAutoId);
                sqlCmd.Parameters.AddWithValue("@fromdate", pobj.fromdate);
                sqlCmd.Parameters.AddWithValue("@todate", pobj.todate);
                sqlCmd.Parameters.AddWithValue("@pollstatus", pobj.pollstatus);
                sqlCmd.Parameters.AddWithValue("@AnsTypeName", pobj.AnsTypeName);
                sqlCmd.Parameters.AddWithValue("@DisplayResults", pobj.DisplayResults);
                sqlCmd.Parameters.AddWithValue("@AllowComments", pobj.AllowComments);
                sqlCmd.Parameters.AddWithValue("@allowGuests", pobj.allowGuests);
                sqlCmd.Parameters.AddWithValue("@allowMultiSelection", pobj.allowMultiSelection);
                sqlCmd.Parameters.AddWithValue("@Status", pobj.Status);

                sqlCmd.Parameters.Add("@isException", SqlDbType.Bit);
                sqlCmd.Parameters["@isException"].Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add("@exceptionMessage", SqlDbType.VarChar, 500);
                sqlCmd.Parameters["@exceptionMessage"].Direction = ParameterDirection.Output;
                SqlDataAdapter sqlAdp = new SqlDataAdapter(sqlCmd);
                pobj.Ds = new DataSet();
                sqlAdp.Fill(pobj.Ds);


                pobj.isException = Convert.ToBoolean(sqlCmd.Parameters["@isException"].Value);
                pobj.exceptionMessage = sqlCmd.Parameters["@exceptionMessage"].Value.ToString();
            }
            catch (Exception ex)
            {
                pobj.isException = true;
                pobj.exceptionMessage = ex.Message;
            }
        }
    }
    public class BAL_CreartPoll
    {
        public static void Insert(PL_CreartPoll pobj)
        {
            pobj.Opcode = 11;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void Getdata(PL_CreartPoll pobj)
        {
            pobj.Opcode = 41;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void Signin(PL_CreartPoll pobj)
        {
            pobj.Opcode = 42;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void Showpoll(PL_CreartPoll pobj)
        {
            pobj.Opcode = 43;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void createvote(PL_CreartPoll pobj)
        {
            pobj.Opcode = 44;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void Displayresult(PL_CreartPoll pobj)
        {
            pobj.Opcode = 45;
            DAL_CreartPoll.ReturnTable(pobj);
        }

        public static void editquestion(PL_CreartPoll pobj)
        {
            pobj.Opcode = 46;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void updatePoll(PL_CreartPoll pobj)
        {
            pobj.Opcode = 47;
            DAL_CreartPoll.ReturnTable(pobj);
        }

        public static void getpolldet(PL_CreartPoll pobj)
        {
            pobj.Opcode = 48;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void updateVote(PL_CreartPoll pobj)
        {
            pobj.Opcode = 49;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void Changests(PL_CreartPoll pobj)
        {
            pobj.Opcode = 50;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void ForgotPassword(PL_CreartPoll pobj)
        {
            pobj.Opcode = 51;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void ResetPassword(PL_CreartPoll pobj)
        {
            pobj.Opcode = 52;
            DAL_CreartPoll.ReturnTable(pobj);
        }
        public static void VerifyUser(PL_CreartPoll pobj)
        {
            pobj.Opcode = 53;
            DAL_CreartPoll.ReturnTable(pobj);
        }
    }
}