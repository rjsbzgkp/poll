﻿using MasterDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for MasterPage
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class MasterPage : System.Web.Services.WebService 
{

    public MasterPage() 
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(EnableSession = true)]
    public string CreatesPoll(PL_CreartPoll pobj)
    {
        if (pobj.Name == "")
        {
            pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"].ToString());
        }
        else
        {
            pobj.Password = Utility.password();
        }
        BAL_CreartPoll.Insert(pobj);
        if ((pobj.exceptionMessage == "Success"))
        {
            var SessionId = Convert.ToInt32(pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString());
            var SessionName = pobj.Ds.Tables[0].Rows[0]["Name"].ToString();
            Session.Add("AutoId", SessionId);
            Session.Add("Name", SessionName);
            return pobj.Ds.GetXml();
        }
        else if ((pobj.exceptionMessage == "Exists"))
        {
            return pobj.exceptionMessage;
        }
        else
        {
            return "false";
        }
    }

    private Func<string> password()
    {
        throw new NotImplementedException();
    }

    [WebMethod(EnableSession = true)]
    public string Login(PL_CreartPoll pobj)
    {
        BAL_CreartPoll.Signin(pobj);
        if (!pobj.isException)
        {
            var SessionId = Convert.ToInt32(pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString());
            var SessionName = pobj.Ds.Tables[0].Rows[0]["Name"].ToString();
            Session.Add("AutoId", SessionId);
            Session.Add("Name", SessionName);
            return pobj.Ds.GetXml();
        }
        else
        {
            return pobj.exceptionMessage;
        }
    }

    [WebMethod(EnableSession = true)]
    public void Logout()
    {
        Session.Abandon();
        Session.RemoveAll();
        Session.Clear();
    }
    [WebMethod(EnableSession = true)]
    public string ForgotPassword(string Email)
    {
        PL_CreartPoll pobj = new PL_CreartPoll();
        try
        {
            pobj.Email = Email;
            BAL_CreartPoll.ForgotPassword(pobj);
            if (!pobj.isException && pobj.Ds.Tables[0].Rows.Count > 0)
            {
                string AutoId= pobj.Ds.Tables[0].Rows[0]["AutoId"].ToString();
                string Name = pobj.Ds.Tables[0].Rows[0]["Name"].ToString();
                string EmailId = pobj.Ds.Tables[0].Rows[0]["Email"].ToString();
                sendForgotMail(Name, Email, AutoId);
                return "success";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            pobj.isException = true;
            pobj.exceptionMessage = ex.Message;
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string VerifyUser(string AutoId)
    {
        PL_CreartPoll pobj = new PL_CreartPoll();
        try
        {
            pobj.AutoId = Convert.ToInt32(AutoId);
            BAL_CreartPoll.VerifyUser(pobj);
            if (!pobj.isException)
            {
                return "true";
            }
            else
            {
                if (pobj.exceptionMessage == "PasswordChanged" || pobj.exceptionMessage == "UnauthorizesAccess")
                {
                    return pobj.exceptionMessage;
                }
                else
                {
                    return "false";
                }
            }
        }
        catch (Exception ex)
        {
            pobj.isException = true;
            pobj.exceptionMessage = ex.Message;
            return ex.Message;
        }
    }
    [WebMethod(EnableSession = true)]
    public string ResetPassword(string AutoId,string Password)
    {
        PL_CreartPoll pobj = new PL_CreartPoll();
        try
        {
            pobj.AutoId = Convert.ToInt32(AutoId);
            pobj.Password = Password;
            BAL_CreartPoll.ResetPassword(pobj);
            if (!pobj.isException)
            {
                return "success";
            }
            else
            {
                return pobj.exceptionMessage;
            }
        }
        catch (Exception ex)
        {
            pobj.isException = true;
            pobj.exceptionMessage = ex.Message;
            return ex.Message;
        }
    }
    public void sendForgotMail(string Name, string Email,string AutoId)
    {
        //var encryptEmail = Encrypt(Email, "POLL-1234-LLOP");
        var htmlString = "";
        htmlString += "<table>"
            + "<tr><td>Dear <b>" + Name + "</b> following is your password reset link.<br><br>"
            + "<a style='color:#fff;background-color:#5cb85c;border-color:#4cae4c;border:1px solid transparent;border-radius:4px;padding: 5px;text-decoration:none;font-size:2rem;' href='https://poll.priorcric.com/ResetPassword.aspx/?asdfgh=" + AutoId + "'>Click here to Reset Password</a><td><tr>"
            + "</table>";
        MailMessage message = new MailMessage();
        SmtpClient smtp = new SmtpClient();
        message.From = new MailAddress("priorcriclive@gmail.com");
        message.To.Add(Email);
        message.Subject = "Poll : Password Recovery";
        message.IsBodyHtml = true;
        message.Body = htmlString.ToString();
        smtp.Port = 587;
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        smtp.UseDefaultCredentials = false;
        smtp.Credentials = new NetworkCredential("priorcriclive@gmail.com", "Priorware@102030");
        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtp.Send(message);
        htmlString = "Success";
    }
    public static string Encrypt(string input, string key)
    {
        byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
        TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        tripleDES.Mode = CipherMode.ECB;
        tripleDES.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = tripleDES.CreateEncryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        tripleDES.Clear();
        return Convert.ToBase64String(resultArray, 0, resultArray.Length);
    }
    public static string Decrypt(string input, string key)
    {
        byte[] inputArray = Convert.FromBase64String(input);
        TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
        tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
        tripleDES.Mode = CipherMode.ECB;
        tripleDES.Padding = PaddingMode.PKCS7;
        ICryptoTransform cTransform = tripleDES.CreateDecryptor();
        byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
        tripleDES.Clear();
        return UTF8Encoding.UTF8.GetString(resultArray);
    }
}
