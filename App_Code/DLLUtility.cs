﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;

public class Utility
{
    public int Opcode { get; set; }
    public DataSet Ds;
    public string exceptionMessage { get; set; }
    public bool isException { get; set; }
    public int PageIndex { get; set; }
    public int PageSize = 10;
    public bool RecordCount { get; set; }
    public int totalRecords { get; set; }
    public int totalPage { get; set; }
    public int startIndex { get; set; }
    public int endIndex { get; set; }

    public static string password()
    {
        string pass = "";
        Random rm = new Random();
        pass = (Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9)) + Convert.ToString(rm.Next(0, 9))).ToString();
        return pass;
    }
}
public class Config
{
    public SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStringCC"].ToString());
}
public class DataEncodeDecode
{
    public string EncodePasswordToBase64(string password)
    {
        try
        {
            byte[] encData_byte = new byte[password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }
        catch (Exception ex)
        {
            throw new Exception("Error in base64Encode" + ex.Message);
        }
    }
    public string DecodeFrom64(string encodedData)//this function Convert to Decord your Password
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        System.Text.Decoder utf8Decode = encoder.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encodedData);
        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        string result = new String(decoded_char);
        return result;
    }
    public string generatePassword()
    {
        int lenthofpass = 4;
        string allowedChars = "1,2,3,4,5,6,7,8,9,0";
        char[] sep = { ',' };
        string[] arr = allowedChars.Split(sep);
        string passwordString = "", temp = "";
        Random rand = new Random();
        for (int i = 0; i < lenthofpass; i++)
        {
            temp = arr[rand.Next(0, arr.Length)];
            passwordString += temp;
        }
        return passwordString;
    }
}
public class gZipCompression
{
    public static void fn_gZipCompression()
    {
        HttpResponse Response = HttpContext.Current.Response;
        string AcceptEncoding = HttpContext.Current.Request.Headers["Accept-Encoding"];
        if (AcceptEncoding.Contains("gzip"))
        {
            Response.Filter = new System.IO.Compression.GZipStream(Response.Filter,
                                      System.IO.Compression.CompressionMode.Compress);
            Response.AppendHeader("Content-Encoding", "gzip");
        }
        else
        {
            Response.Filter = new System.IO.Compression.DeflateStream(Response.Filter,
                                      System.IO.Compression.CompressionMode.Compress);
            Response.AppendHeader("Content-Encoding", "deflate");
        }
    }
}

