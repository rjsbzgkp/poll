﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using MasterDLL;

public partial class PollList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string text = File.ReadAllText(Server.MapPath("/CustomJS/PollList.js"));
        Page.Header.Controls.Add(
             new System.Web.UI.LiteralControl(
                "<script>" + text + "</script>"
            ));
    }

    [WebMethod(EnableSession = true)]
    public static string Getalldata(PL_CreartPoll pobj)
    {
        if (Convert.ToInt32(System.Web.HttpContext.Current.Session["AutoId"]) > 0)
        {
            //PL_CreartPoll pobj = new PL_CreartPoll();
            pobj.AutoId = Convert.ToInt32(HttpContext.Current.Session["AutoId"].ToString());
            BAL_CreartPoll.Getdata(pobj);
            if (!pobj.isException)
            {
                return pobj.Ds.GetXml();
            }
            else
            {
                return "false";
            }
        }
        else
        {
            return "Session Expired";
        }
    }
}